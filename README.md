# BLVD Serverless Base Package
This package serves as the launching pad for a new BLVD serverless lambda function.
It provides helper functions to access the BLVD database and make GraphQL calls.

## Setup
1. Fork the repo  
   - First fork the repository to begin creating a new function

2. Setup dependencies  
   - Run `npm install` to install local dependencies  
   - Ensure you have installed the [aws-cli](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) and [configured same](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)

3. Rename function  
Ensure that the function is renamed in the following files by updating the `name` value
   - package.json  
   - package-lock.json  
   - serverless.ts  

4. Create your function(s)  
   - Modify or add a new key to the `functions` object in `serverless.ts`  
   - Create functionality for your new function

5. Test your function locally
   - Invoke your function in your local environment by running `serverless invoke local --function {function_name}`  
   - To send a test payload create a json file with the desired data within a `.event` folder. This folder will not be tracked in source control. Pass this file to the local instance of your function by running `serverless invoke local --function {function_name} -p .event/{filename}.json`

6. Deploy your function
   - When your functionality is complete deploy your function to AWS by running `npm run deploy`  
   - Make a note of the API endpoint(s) returned at the end of the deployment. You can also retrieve the functions endpoints by running `npx serverless deploy list`

## Accessing the database
1. Create a `.env` file with the database credentials  
   - Copy the `.env-template` file and rename it to `.env`  
   - Complete the different fields with the credentials for the database

2. Add the `vpc` key to your function in [`serverless.ts`](/serverless.ts)
   - This ensures your function is provisioned to access the database  
   - See the `test` function in [`serverless.ts`](/serverless.ts) for an example

3. Access the BLVD database using the exported functions in `common/sql.ts`

> **Note:**   
> If on executing a sql call locally you receive the error
> `{"code":"PROTOCOL_SEQUENCE_TIMEOUT","fatal":true,"timeout":10000}`  
> Ensure that your IP address is present and correct in the [VPC Security Group](https://console.aws.amazon.com/vpc/home?region=us-east-1#SecurityGroup:groupId=sg-b06c58c7)

## Returning data
To easily return json data from a function call a convenient helper function has been created in [`common/api-responses.ts`](/common/api-responses.ts).
See [`hello.ts`](/handlers/hello.ts) for an example of its usage.

## Debugging
To debug your functions using VSCode you can use the included launch scripts in [.vscode/launch.json](/.vscode/launch.json).  
   - The invoke local script can be modified and copied for each function written.  
   - The offline script will create a server with the different function endpoints to allow testing from a browser or other REST client. This will also watch for changes and update on each file save
> **Note:** There is an open bug that affects serverless offline reloading [#931](https://github.com/dherault/serverless-offline/issues/931).  
> This only currently affects gql dependent functions due to its effect on aws-appsync.  
> As such you we have enabled useChildProcesses in the [serverless.ts](/serverless.ts) file. This has the potential to create a memory leak if the code is rebuilt multiple times.  
> If you notice a significant decrease in your performance after multiple changes just end and restart the debug process to clear child processes
