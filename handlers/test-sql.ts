import type { APIGatewayProxyHandler } from 'aws-lambda';
import { apiResponse } from '../common/api-response';
import { executeSQL } from '../common/sql';

export const testSQL: APIGatewayProxyHandler = async () => {
  try {
    const [config] = await executeSQL('SELECT * FROM Configs', true);
    return apiResponse.success({config});
  } catch (error) {
    return apiResponse.failure({error});
  }
}