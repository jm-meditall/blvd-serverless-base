import { APIGatewayProxyHandler } from 'aws-lambda';
import gql from 'graphql-tag';

import { apiResponse } from '../common/api-response';
import { graphqlClient } from '../common/graphql';

export const testGQL: APIGatewayProxyHandler = async () => {
  try {
    const query = gql(`
      query GetConfig {
        getConfig {
          bankSize
          cooldown
          upgradeMessage
          inMaintenance
          maintenanceMessage
          dbVersion
          gqlVersion
          showUpdate
          minBuild
          crownBonus
        }
      }
    `);
    const result = await graphqlClient.query({query});
    return apiResponse.success({result});
  } catch (error) {
    return apiResponse.failure({error})
  }
}