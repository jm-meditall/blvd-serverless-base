import 'source-map-support/register';

import { APIGatewayProxyHandler } from 'aws-lambda';

import { apiResponse } from '../common/api-response';

export const hello: APIGatewayProxyHandler = async (event, _context) => {
  return apiResponse.success({
    message: 'Go Serverless Webpack (Typescript) v1.0! Your function executed successfully!',
    input: event,
  });
}
