import mysql from 'mysql';

export const pool = mysql.createPool({
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
  host     : process.env.ENDPOINT,
  user     : process.env.USERNAME,
  password : process.env.PASSWORD,
  database : process.env.DBNAME,
  multipleStatements: true,
  timezone:'Z',
  typeCast: ( field, useDefaultTypeCasting ) => {
    // We only want to cast bit fields that have a single-bit in them. If the field
    // has more than one bit, then we cannot assume it is supposed to be a Boolean.
    if ( ( field.type === "BIT" ) && ( field.length === 1 ) ) {
        const bytes = field.buffer();
        // A Buffer in Node represents a collection of 8-bit unsigned integers.
        // Therefore, our single "bit field" comes back as the bits '0000 0001',
        // which is equivalent to the number 1.
        return( bytes[ 0 ] === 1 );
    }
    return( useDefaultTypeCasting() );
  }
});

export function populateAndSanitizeSQL(sql: string, variableMapping: {[key: string]: any}) {
  Object.entries(variableMapping).forEach(([key, value]) => {
    const escapedValue = pool.escape(value);
    const reg = new RegExp(key, 'g');
    sql = sql.replace(reg, escapedValue);
  });

  return sql;
}

export async function executeSQL<T = any>(sql: string, log: boolean = false): Promise<T> {
  if (log) {
    console.log('Executing SQL:', sql);
  }

  return new Promise((resolve,reject) => {
    pool.query(sql, (err, data: T) => {
      if (err) {
        console.log('SQL error:\n'+JSON.stringify(err));
        return reject(err);
      }
      else {
        if (log) {
          console.log('SQL returned:\n'+JSON.stringify(data));
        }

        return resolve(data);
      }
    } );
  });
}
