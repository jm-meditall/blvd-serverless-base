import 'cross-fetch/polyfill';

import AWSAppSyncClient from 'aws-appsync';

const {
  GQL_HOST, GQL_REGION,
  AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_SESSION_TOKEN
} = process.env;

export const graphqlClient = new AWSAppSyncClient({
  url: `${GQL_HOST}/graphql`,
  region: GQL_REGION,
  auth: {
    type: 'AWS_IAM',
    credentials: {
      accessKeyId: AWS_ACCESS_KEY_ID,
      secretAccessKey: AWS_SECRET_ACCESS_KEY,
      sessionToken: AWS_SESSION_TOKEN
    }
  },
  disableOffline: true
});