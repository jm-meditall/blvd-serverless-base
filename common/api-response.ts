import type { APIGatewayProxyResult } from "aws-lambda";

export type ApiResponseOptions = Omit<APIGatewayProxyResult, 'statusCode' | 'body'>;
export type ApiResponseFunction = (body: Record<string, any> | string | number, options?: ApiResponseOptions) => APIGatewayProxyResult;

function apiResponseFor(statusCode: number): ApiResponseFunction {
  return (body, options) => {
    return {
      statusCode,
      body: JSON.stringify(body, null, 2),
      ...options
    };
  }
}

/** 
 * Send an function api response
 * @see ApiResponse
 **/
export const apiResponse = {
  success: apiResponseFor(200),
  failure: apiResponseFor(400)
};