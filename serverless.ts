import type { Serverless, Vpc } from 'serverless/aws';

const vpc: Vpc = {
  securityGroupIds: ["sg-b06c58c7"],
  subnetIds: [
    "subnet-b1f2b9ec",
    "subnet-04695060"
  ]
};

const serverlessConfiguration: Serverless = {
  service: {
    name: 'blvd-serverless-base',
  },
  frameworkVersion: '1',
  custom: {
    webpack: {
      webpackConfig: './webpack.config.js',
      includeModules: true
    },
    "serverless-offline": {
      useChildProcesses: true
    }
  },
  package: {
    individually: true
  },
  // Add the serverless-webpack plugin
  plugins: [
    "serverless-webpack",
    "serverless-dotenv-plugin",
    "serverless-offline"
  ],
  provider: {
    name: 'aws',
    runtime: 'nodejs12.x',
    apiGateway: {
      minimumCompressionSize: 1024,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1'
    },
    iamRoleStatements: [{
      Effect: 'Allow',
      Action: [
        "appsync:GraphQL",
        "logs:CreateLogStream",
        "logs:CreateLogGroup",
        "logs:PutLogEvents"
      ],
      Resource: "*"
    }]
  },
  functions: {
    hello: {
      handler: 'handlers/hello.hello',
      events: [
        {
          http: {
            method: 'get',
            path: 'hello',
          }
        }
      ]
    },
    testSQL: {
      handler: 'handlers/test-sql.testSQL',
      events: [
        {
          http: {
            method: 'get',
            path: 'test-sql',
          }
        }
      ],
      vpc
    },
    testGQL: {
      handler: 'handlers/test-gql.testGQL',
      events: [
        {
          http: {
            method: 'get',
            path: 'test-gql',
          }
        }
      ],
      vpc
    }
  }
}

module.exports = serverlessConfiguration;
